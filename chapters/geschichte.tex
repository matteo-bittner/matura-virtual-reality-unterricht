\selectlanguage{ngerman}%
\chapter{Geschichte und Entwicklung von VR}%
\label{chap:geschichte}

Die virtuelle Realität ist eine Technologie, an der heute noch intensiv
geforscht wird. Sie eröffnet ihren Benutzern viele Möglichkeiten.
Chancen, die sich in der Zukunft noch massiv erweitern werden. Doch wie
hat alles angefangen?

\section{Definition}

Wenn man die Definition mithilfe einer Suchmaschine nachschlägt, kommt man
in vielen Fällen, auf folgendes Ergebnis: \glqq{}Die virtuelle Realität ist
eine computergenerierte Wirklichkeit mit Bild (3D) und in vielen Fällen auch
Ton\grqq{} \citep{gabler:wirtschaftslexikon}. Jedoch, hatte die Idee von
virtueller Realität schon in früherer Zeit, in welcher Computer noch nicht
existiert hatten, verschiedene Personen inspiriert, welche infolge analoge
Ideen entworfen und kreiert haben.

\section{Meilensteine}

Man könnte sagen, dass das Interesse für eine virtuelle Realität sehr
früh in Form einer immersiven Unterhaltung entstand, und zwar bereits
im 16.~Jahrhundert. Es braucht allerdings bis zum Jahr 1962, bis wir
ein erstes digitales Exemplar zu sehen bekommen, das Sensorama, ein
4D-Kino. Schauen wir uns die wichtigsten Meilensteine der Entwicklung
von \acrshort{VR} näher an.
\citep{vrs:history,coursera:analoguevirtualreality}.

\subsection{Die analoge virtuelle Realität}

\subsubsection{Trompe l'oeil}

Das Trompe l'oeil, ist eine Art von Gemälden, welche in 3d gezeichnet werden.
Ihre Besonderheit ist, dass das Bild real aussieht.

\begin{figure}[ht]
\centering
\begin{minipage}{.35\linewidth}
  \includegraphics[width=\linewidth]{violinen-illusion.jpg}
  \captionof{figure}{Illusion einer hängenden Geige hinter einer Tür. Savardi \& Kubovy \& Bianchi. (2011).Trompe l'oeil of a violin and bow painted on a door of Chatsworth House (ca. 1723) by Jan van der Vaart }
  \label{fig:violine-illusion}
\end{minipage}
\hspace{.05\linewidth}
\begin{minipage}{.5\linewidth}
  \includegraphics[width=\linewidth]{leicester-square-panorama.jpg}
  \captionof{figure}{Legende von Leicester Square Panorama. Alice. (2015). Leicester Square Panorama for Robert Barker by Robert Mitchell, 1801(building from 1792)}
  \label{fig:leicester-panorama}
\end{minipage}
\end{figure}

Zum Beispiel wie in \autoref{fig:violine-illusion}
denkt man das hinter der Tür sich noch eine Tür befindet, an welcher eine Violine
festgemacht wurde. Diese Violine hat die Schatten, welche auch bei einer realen Violine
mit dieser Beleuchtung, vorhanden wären. Auf den ersten Blick scheint sie griffbereit
dort zu hängen, doch wenn man sie genauer betrachtet merkt man, dass es nur ein Bild ist.
Dieses Kunstwerk findet man im Chatsworht House in Derbyshire(England).

Ein anderes Beispiel wäre die \autoref{fig:leicester-panorama}. Hier ist es zwar kein
Objekt, welches griffbereit scheint, sondern ein ganzes Panoramabild, das den Eindruck
vermittelt, an einem anderen Ort zu sein. Dies ist ein Beispiel für immersive Unterhaltung
des 18. und 19. Jahrhunderts, vergleichbar mit dem, was wir heute durch Head-Mounted
Displays (\acrshort{HMD}s) erleben können. Solche Kunstwerk  waren auch Inspiration
für nachfolgende Erfinder. Dieses Panorama, welches im Jahre 1801 von Robert
Mitchell vollendet wurde, befindet sich im Leicester Square in London.
\citep{vrs:history,coursera:analoguevirtualreality}.

\subsubsection{Das Stereograph}

\begin{figure}[ht]
  \centering
 \begin{minipage}{.4\linewidth}
    \includegraphics[width=\linewidth]{stereo-photography.png}
    \captionof{figure}{Stereograph mit Bild. George Eastman Museum. (2016). Stereographs}
  \label{fig:stereograph}
 \end{minipage}
 \hspace{.05\linewidth}
 \begin{minipage}{.5\linewidth}
    \includegraphics[width=\linewidth]{blog-robbett-mary-kate-2018-04-05-stereograph-as-an-educator-loc-banner-edit.jpg}
    \captionof{figure}{Bild für ein Stereograph. Wikipedia. (2024). Underwood \& Underwood: The stereograph as an educator, Stereoskopie von 1901}
  \label{fig:bild-stereograph}
 \end{minipage}
\end{figure}

Mit diesem Objekt, namens Stereograph, kommen wir der virtuellen Realität schon näher.
Der Stereograph entstand kurz nach der Erfindung der Fotografie. Wie man in der Abbildung
sehen kann, besteht der Stereograph aus einer Brille und einem Brett, auf welches man
zwei fast identische Bilder, die von zwei leicht verschobenen Sichtpunkten gemacht
wurden, hinstellt. In vielen Fällen sind die Bilder auch noch gekrümmt.
Sobald man die Distanz, zwischen der Brille und den Bildern, richtig
eingestellt hat und durch die Brille schaut, erscheint ein 3D Bild. Dieses Gerät
wurde schnell sehr populär, denn im 20. Jahrhundert waren Reisen keine Selbstverständlichkeit
für jedermann, dies lag am einfachen Grund, das reisen sehr teuer waren.
Mit dieser Erfindung konnten die ärmeren Leute die berühmtesten architektonischen
Kunstwerke in 3D-Form erblicken. Nach und nach wurden Stereographen auch in
Bibliotheken zur Verfügung gestellt.
\citep{vrs:history,coursera:analoguevirtualreality}.

\subsubsection{Link-Trainer}

\begin{figure}[ht]
  \centering
  \includegraphics[width=.7\linewidth]{link-trainer-shuttleworth-collection.jpg}
  \captionof{figure}{Puppe im Link-Trainer. New. (2018). Edwin Link's 1929 ANT-18 Link Trainer}
  \label{fig:link-trainer}
\end{figure}

Edward Link schuf 1929 den Link-Trainer, die erste Flugsimulationsmaschine zur
Pilotenausbildung. Ursprünglich als Unterhaltungsgerät auf Jahrmärkten und in
Spielhallen vermarktet, betrachtete das Militär diese Maschine anfänglich
lediglich als Spielzeug und lehnte ihren Einsatz in der Pilotenausbildung ab.
Seit dem Zweiten Weltkrieg sind Flugsimulatoren integraler Bestandteil der
virtuellen Realität. Verschiedene Unternehmen, darunter Evans \& Sutherland,
haben sich auf die Entwicklung von Flugsimulatoren spezialisiert.
\citep{vrs:history,coursera:analoguevirtualreality}.

\subsection{Virtuelle Realität in der virtuellen Zeit}

\subsubsection{Sensorama Simulator}

\begin{figure}[ht]
  \centering
  \includegraphics[width=.7\linewidth]{morton-heilig-sensorama.jpg}
  \captionof{figure}{Legende vom Sensorama. Elaine. (2016). Morton Heilig's ``Sensorama'', a personal theater cabinet}
  \label{fig:sensorama}
\end{figure}

Das Sensorama repräsentiert eines der frühesten Beispiele für immersive,
multisensorische Technologie. Diese wegweisende Technologie wurde 1962 von
Morton Heilig entwickelt und zählt zu den ersten Virtual-Reality-Systemen (VR).
Der Sensorama-Simulator bot eine vielfältige Palette von Effekten. Nutzer
tauchten in das System ein, indem sie ihren Kopf vor einen Bildschirm hielten,
auf dem ein Stereo-Weitwinkelfilm abgespielt wurde. Zusätzlich zu visuellen
Reizen erhielten die Nutzer auch 3D-Klänge sowie Wind- und Geruchsimpressionen.
Darüber hinaus erstellte das Gerät leichte haptische Bewegungen. Diese Innovation
demonstriert eindrucksvoll die Spitzenleistungen analoger Technologien, die
Filmemachern und Kreativen zur Verfügung standen.
\citep{vrs:history,coursera:digitalvirtualreality,wikipedia:sensorama}.

\subsubsection{Sword of Damocles System}

Das Sword of Damocles System wurde im Jahr 1968 entwickelt und wird von vielen
als das erste digitale Virtual-Reality-System angesehen. Es umfasst ein
kopfmontiertes Display, das an einem mechanischen Gestell von der Decke herabhängt.
Obwohl es bereits über die grundlegenden Funktionen verfügte, die heute mit
Virtual-Reality-Systemen verbunden sind, war es technisch noch sehr rudimentär.
Das kopfmontierte Display verfolgte die Bewegungen des Nutzers und passte die
Grafiken entsprechend an, um eine Ich-Perspektive zu simulieren. Trotz der damals
noch begrenzten grafischen Darstellung mit Y-Frame-Grafiken und niedriger Auflösung,
legte das System den Grundstein für spätere Entwicklungen von Head-Mount-Displays.
\citep{vrs:history,coursera:digitalvirtualreality}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.6\linewidth]{sword-of-damocles-system.png}
  \captionof{figure}{Herr verbunden mit dem Sword of Damocles. Elaine.(2016). ``Sword of Damocles'' VR Headset devloped by Ivan Sutherland}
  \label{fig:sword-damocles}
\end{figure}

\subsubsection{VIEW(Virtual Interface Environment Workstation)}

Das System VIEW Workstation der NASA wurde in den 1980er Jahren entwickelt und
stellt gewissermassen einen Vorläufer heutiger Virtual-Reality-Systeme dar. Es
handelt sich um ein tragbares stereoskopisches Anzeigesystem, das es dem Benutzer
 ermöglicht, in eine künstlich generierte Computerumgebung oder eine reale
 Umgebung einzutauchen, die durch entfernte Videokameras übertragen wurde.
 Der Benutzer ist in der Lage, sich frei zu bewegen und trägt Kopfhörer sowie
 Handschuhe, die eine präzise Erfassung seiner Fingerbewegungen ermöglichen.
 Zudem kann das System die Blickrichtung des Benutzers verfolgen. Die NASA nutzte
 dieses System für diverse Projekte, darunter die Schulung von Astronauten.
 In den 1990er Jahren markierte diese Technologie gewissermassen den Beginn
 einer neuen Industrie, bevor Teile davon von der Firma VPL übernommen wurden.
 \citep{vrs:history,coursera:digitalvirtualreality}.

 \begin{figure}[ht]
  \centering
  \includegraphics[width=.8\linewidth]{view-workstation-from-nasa.jpg}
  \captionof{figure}{Dame mit einem VIEW aufgessetzt. Rosson. (2014). The Virtual Interface Environment Workstation (VIEW), 1990}
  \label{fig:view}
\end{figure}


\subsubsection{Virtual research flight helmet}

\begin{figure}[ht]
  \centering
  \includegraphics[width=.7\linewidth]{virtualresearchflighthelmet.png}
  \captionof{figure}{Flight Helmet. Brown. (o.D.). Virtual Research Flight Helmet}
  \label{fig:virtual-flight}
\end{figure}

Der Virtual Research Flight Helmet ist ein Virtual-Reality-Headset, das von
Unternehmen Virtual Research hergestellt wurde. Es wurde erstmals im August 1991
eingeführt und markierte das Debüt des Unternehmens im Bereich der VR-Headsets.
Der Virtual Research Flight Helmet wurde speziell für den Einsatz in
Avionik-Simulationen konzipiert.
Dieses Headset erforderte eine Verbindung zu einem PC, um genutzt werden zu
können, nutzte ein nicht positionsabhängiges 3D-Tracking und wies eine
vergleichsweise niedrige Auflösung pro Auge von 360x240 auf.
Ein zentrales Problem bestand darin, dass die Systeme erhebliche Rechenleistung
erforderten, die zu jener Zeit, insbesondere bei Heimcomputern, nicht ausreichend
vorhanden war. 3D-Grafikbeschleuniger waren damals nur selten verfügbar.
Zusätzlich wogen die Headsets 1670 g und waren kostenintensiv.
In den folgenden zehn Jahren sanken die Kosten für derartige Systeme, während
gleichzeitig Bestrebungen unternommen wurden, auch im Bereich der
Unterhaltungselektronik Fuss zu fassen.
\citep{vrs:history,coursera:digitalvirtualreality}.

\subsubsection{CAVE}

CAVE ist ein Akronym, das für Cave Automatic Virtual Environment steht. Die erste
CAVE wurde von Carolina Cruz-Neira, Daniel Sandin und Thomas De Fanti an der
Universität Illinois entwickelt.
Für eine präzise Manipulation von Objekten ist eine klare Vorstellung von ihrer
Position und Handhabung erforderlich. Sowohl CAVE-Systeme als auch Head-Mounted
Displays vermitteln ein realistisches Gefühl von Nähe und räumlicher Tiefe der
betrachteten Objekte. Bei entsprechendem Budget besteht die Möglichkeit, ein
hochwertiges Head-Mounted Display zu erwerben. Verschiedene Unternehmen
produzierten kopfgetragene Displays, um die Bedürfnisse der Industrie und des
akademischen Sektors nach hochwertigen Systemen zu erfüllen.
Im Gegensatz zu einem Head-Mount Display, bei dem die Bilder direkt auf dem Kopf
getragen werden, sind bei einem CAVE-ähnlichen System die Bilder um den Betrachter
herum an den Wänden positioniert. Dies erfordert eine fortlaufende Aktualisierung
der Bilder an den Wänden durch den Computer. Ein Head-Tracker bleibt dennoch
unerlässlich. Ein wesentlicher Vorteil dieser Systeme liegt in der minimalen
Latenzzeit, sodass Bewegungen des Kopfes nahezu verzögerungsfrei umgesetzt werden.
Eine typische CAVE besteht aus drei Wänden und einem Boden, wobei jeder Wand ein
eigener Computer zugeordnet werden kann. Durch die Verwendung mehrerer Computer
lässt sich die Grafikleistung und -qualität steigern. CAVEs sind in Laboren, die
sich mit virtueller Realität beschäftigen, sowie in spezifischen
ingenieurwissenschaftlichen Disziplinen, wie beispielsweise bei Jaguar Land Rover
im Vereinigten Königreich, weit verbreitet. Dort werden CAVE-ähnliche Systeme
genutzt, um verschiedene Aspekte von Fahrzeugen zu analysieren, beispielsweise
die Benutzerinteraktion mit dem Auto, einschliesslich Lenkung sowie Be- und
Entladen des Kofferraums. Diese Technologie bietet einen klaren Vorteil bei der
Bewältigung komplexer, dreidimensionaler Aufgaben.
Für eine präzise Manipulation von Objekten ist eine klare Vorstellung von ihrer
Position und Handhabung erforderlich. Sowohl CAVE-Systeme als auch HMD
vermitteln ein realistisches Gefühl von Nähe und räumlicher Tiefe der
betrachteten Objekte. Bei entsprechendem Budget besteht die Möglichkeit, ein
hochwertiges HMD zu erwerben. Verschiedene Unternehmen
produzierten kopfgetragene Displays, um die Bedürfnisse der Industrie und des
akademischen Sektors nach hochwertigen Systemen zu erfüllen.
\citep{vrs:history,coursera:digitalvirtualreality}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.7\linewidth]{cave.jpg}
  \captionof{figure}{Beispiel eines CAVE-Raums. Wikipedia. (2024). The CAVE}
  \label{fig:cave}
\end{figure}

\subsubsection{Sony}

Schon im Jahre 1997 hat Sony ihr erstes \acrshort{HMD} entwickelt, welches \glqq{}Glasstron\grqq{}
genannt wurde. 2002 erschien das \glqq{}PUD-J5A\grqq{}, aber es war nur im japanischen Sonystore
verfügbar. Für den Weltmarkt konzipierte Sony, im Jahre 2016, ein VR-Headset, welches
in Kombination mit der PS4-Konsole, welche schon sehr verbreitet war, zum Funktionieren
gebracht werden konnte. Dieses Produkt veränderte bei einigen Gamern die Spielweise.
\citep{wikipedia:playstationvr}

\subsubsection{Oculus/Meta}

\begin{figure}[ht]
  \centering
  \includegraphics[width=.7\linewidth]{meta-quest-3.jpg}
  \captionof{figure}{Komplettes Set von der Meta Quest 3. Digitec. (2024). Meta Quest 3}
  \label{fig:meta-quest}
\end{figure}

Oculus, eine Firma gegründet von Palmer Luckey und 2 Jahre später gekauft von Facebook,
ist mit Sony ein Marktführer in der Kategorie der virtuellen Realität. Sie materialisierten
ihr erstes Massenproduktions-Konzept im Jahre 2013, dessen Sinn es war verfügbar für alle
zu sein, indem es zu einem leistbaren Preis verkauft wurde, und trotzdem sehr gute Qualität
behielt. Der Name des Produktes ist \glqq{}Oculus Rift\grqq{}. Später
brachten sie weitere bessere Modelle, zum Beispiel \glqq{}Oculus Rift
S\grqq{} oder die modernste Gruppe \glqq{}Oculus Quest\grqq{}.

Die heutige von Facebook gekaufte Oculus heisst \product{Meta Quest}. Die \product{Meta Quest 3}
ist das neuste Produkt, welches Meta bis jetzt vorgestellt hat. Beim Kauf, bekommt man
gewöhnlicherweise zwei Controller, ein Steuerungsgerät für jede Hand, mitgeliefert.
Diese müssen aber nicht immer verwendet werden,
denn man kann auch beide Hände als Maus verwenden. Die Einrichtung des Geräts ist ziemlich
intuitiv und einfach, vor allem wenn man das einen Jugendlichen überlässt. Sobald man die
Quest eingerichtet hat, wird es sofort zu einem \acrshort{AR}-Erlebnis. Im Sichtfeld des
Nutzers wird eine Appleiste visualisiert mit einem Browser, Appstore und paar andere
nützliche Applikationen. Die weiteren Angebote kann der User sich entweder im virtuellen
Appstore herunterladen, oder mit dem Mobilgeräte einfach über die Meta Quest mobile App.
Dieses Erlebnis hatte der Autor dieser Arbeit bei seiner Nutzung der \product{Meta Quest 3}.
\citep{wikipedia:oculus,wikipedia:facebookaquisitions}

\subsubsection{Apple Vision Pro}%
\label{subsec:apple-vision-pro}

Virtual Reality (VR) in einem einzigen, fortschrittlichen Gerät vereint.
Es wurde im Juni 2023 vorgestellt und markiert Apples ersten bedeutenden
Vorstoss in den Bereich des räumlichen Computings. Dieses \acrshort{MR}-Gerät
ist darauf ausgelegt, digitale Inhalte mit der realen Welt zu
verschmelzen und bietet eine breite Palette an Funktionen, die von
persönlicher Produktivität über Unterhaltung bis hin zu Kommunikation und
professionellen Anwendungen reichen.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.6\linewidth]{apple-vision-pro.jpg}
  \captionof{figure}{Dame mit aufgesetzter Apple Vision Pro. Haslam \& Cross. (2024). Apple}
  \label{fig:apple-vision}
\end{figure}

Ausschlaggebend sind, wie typisch bei Apple, die Einfachheit der Bedienung
(z.B. die Benutzung der Augen als Maus), die realistische Darstellung
(z.B. die virtuellen Monitore wirken wie echt und unbeweglich im Raum
platziert, die Objekte haben sogar einen Schatten) und die nahtlose
Integration mit anderen Geräten. Damit ist es möglich, durch die Brille
alle Monitore in den Büros zu ersetzen bzw. die Fähigkeiten von Macbooks
nützlich zu erweitern.
\citep{apple:vision-pro,youtube:techdale}
