\selectlanguage{ngerman}%
\chapter{Grundlagen der virtuellen Realität}%
\label{chap:grundlagen}

\section{Arten von Realitäten}

Wir unterscheiden zwischen 4 Arten von Realitäten: VR, AR, MR, XR

\subsection{Virtual Reality}

Virtuelle Realität kann als computergenerierte virtuelle Umgebung
beschrieben werden, die darauf abzielt, die reale Umgebung des Benutzers
zu ersetzen. In diesem Fall existiert nur noch die virtuelle Sichtweise
und nicht mehr die reale Welt. Die Realität in der virtuellen Realität
ist daher nicht die tatsächliche Welt, sondern eine synthetische,
computergenerierte Welt.

Die virtuelle Realität wird durch drei Hauptmerkmale charakterisiert.
Erstens betont die virtuelle Realität Autonomie und Handlungsfähigkeit,
wobei der Benutzer stets die Kontrolle behält. Dies wird durch
Head-Tracking und die Verwendung des Körpers als Eingabegerät erreicht.
Durch das Tragen eines Headsets wird das Gerät im 3D-Raum verfolgt,
während der Körper als Eingabegerät genutzt werden kann. Dies geschieht
oft mithilfe von Virtual-Reality-Controllern. Es wird jedoch zunehmend
beliebter, die Hände als Eingabegeräte zu verwenden. Neben den Händen
kann auch die Sprache als Eingabegerät dienen.

Dies führt uns zum zweiten Hauptmerkmal, nämlich dass die virtuelle
Realität eine natürliche Interaktion ermöglicht, bei der Gesten mit dem
Controller, den Händen oder den Fingern sowie die Sprache als
Eingabemöglichkeiten genutzt werden.

Das dritte Merkmal der virtuellen Realität ist, dass man ein Gefühl der
Präsenz vermittelt, ein Gefühl des \glqq{}Dabeiseins\grqq{}. Wenn man also
in die virtuelle Welt eintaucht, erlebt man ein intensives Gefühl der
Anwesenheit. Dieses Eintauchen wird durch die Stimulation mehrerer
unserer Sinne erreicht. \citep{corsera:platforms}

\subsection{Augmented Reality}

Augmented Reality erweitert die Realität. Der Nutzer sieht also sowohl die
reale Welt als auch die virtuelle Welt. Sie werden quasi miteinander
verschmolzen. Die reale und die virtuelle Welt kann man als
\glqq{}zusammengesetzte Ansicht\grqq{} definieren. Das Konzept der
\glqq{}Reality\grqq{} in AR bezieht sich auf die uns umgebende reale Welt.

Augmented Reality wird durch drei Hauptmerkmale definiert. Erstens
kombiniert Augmented Reality reale und virtuelle Objekte, um eine
gemeinsame Ansicht zu schaffen. Diese Ansicht umfasst nicht nur visuelle
Elemente, sondern kann auch Audioinformationen einbeziehen. Beim Tragen
einer Augmented-Reality-Brille bleibt die reale Welt sichtbar, während
virtuelle Inhalte über sie projiziert werden. Das zweite Hauptmerkmal von
Augmented Reality besteht darin, dass sie in Echtzeit interaktiv ist, was
sowohl explizite als auch implizite Interaktion ermöglicht. Explizite
Interaktion beinhaltet Gesten und Sprache, während implizite Interaktion
durch Bewegungen des Geräts erfolgt, um virtuelle Inhalte nahtlos in die
reale Welt zu integrieren. Das dritte Merkmal von Augmented Reality
besteht darin, dass sie in 3D erfasst wird, um reale und virtuelle Objekte
miteinander zu verbinden und sie in der physischen Welt als
zusammengehörig erscheinen zu lassen. Zusammengefasst sind dies die drei
Hauptmerkmale von Augmented Reality: die Überlagerung virtueller Inhalte
über die reale Welt, wodurch die reale Welt weiterhin sichtbar bleibt und
gleichzeitig virtuelle Elemente hinzugefügt werden. \citep{corsera:platforms}

\subsection{Augmented Virtual Reality}

Augmented Virtual Reality stellt eigentlich das Gegenteil von AR dar, da
es hauptsächlich virtuell ist und das Virtuelle mit physischen Inhalten
erweitert. In VR sind beispielsweise die physischen Hände nicht sichtbar,
stattdessen sieht man die Controller, die eine gute Simulation der
Handbewegungen und ihrer Position im 3D-Raum bieten. Dies kann als eine
Form erweiterter Virtualität betrachtet werden, bei der reale
Weltkoordinaten integriert werden, einschliesslich der Pose und Bewegung
der Hände in die virtuelle Umgebung. \citep{corsera:platforms}

\subsection{Mixed Reality}

Mixed Reality ist ein Begriff, der oft für Verwirrung sorgt. Tatsächlich
wird dies oft anders definiert. Wir definieren gemischte Realität als die
Verschmelzung der physischen und der virtuellen Welt mit unterschiedlichen
Graden der Augmentation. \citep{corsera:platforms}

\subsection{Extended Reality}

\acrshort{XR} (Extended Reality) ist ein Begriff, der häufig
verwendet wird, wobei X nur ein Platzhalter für Augmented,
Virtual oder Mixed Reality ist. \citep{corsera:platforms}

\section{Vier Klassen der XR-Technologie}

Diese vier Klassen sind die Grundpfeiler der erweiterten Realität
(XR), die eine breite Palette von Erfahrungen und Möglichkeiten
bieten: Geräte (Hardware), Plattformen, Anwendungen und Werkzeuge.
\citep{corsera:platforms}

\subsection{Hardware}

Dies umfasst die Hardware, die zur Nutzung von XR-Technologien
erforderlich ist. Dazu gehören Virtual-Reality-Headsets (VR),
Augmented-Reality-Brillen (AR), Mixed-Reality-Geräte (MR) und
andere tragbare Technologien, die immersive Erlebnisse ermöglichen.
Wir können zwischen Standalone/Built-in und Tethered XR-Geräten
unterscheiden. Standalone Geräte verfügen über integrierte
XR-Technologien, sind also vollständig eigenständige Computer.
Wie die Meta Quest 3, die Vision Pro, die Microsoft HoloLens 2.


Tethered und Adapter-Devices sind Geräte welche mit zum Beispiel
einem Gummiband ein Smartphone festbinden. Das Google Cardboard,
oder die an einen Computer angeschlossen werden müssen (verkabeln),
wie die Oculus Rift und HTC Vive. \citep{google:google-cardboard}

\subsubsection{Beispiele von XR-Geräten}

\paragraph{Google Cardboard}

Google Cardboard ist ein preisgünstiges VR-Headset, das von
Google entwickelt wurde, um Benutzern die Möglichkeit zu geben, mithilfe
ihres Smartphones in die Welt der Virtual Reality einzutauchen. Die Idee
hinter Google Cardboard ist es, Virtual Reality durch ein einfaches und
erschwingliches Gerät einer breiteren Öffentlichkeit zugänglich zu machen.

Um Google Cardboard nutzen zu können, wird ein kompatibles Smartphone
benötigt, das in das Headset eingesetzt wird. Das Headset verfügt über
Linsen, die das Smartphone-Bild so anpassen, dass ein 3D-Effekt erzeugt
wird.

\begin{figure}[ht]
    \centering
    \includegraphics[width=.5\linewidth]{google-cardboard.jpg}
    \captionof{figure}{Google Cardboard. Ammerman. (2020). Google Cardboard}
    \label{fig:google-cardboard}
\end{figure}

Das Headset kann selbst hergestellt werden. Ein Hersteller-Kit mit
technischen Spezifikationen, Zeichnungen für Linsen, Leiterplatten,
Stanzlinien und mehr sowie Anleitungen für Qualitätssicherung und
Produktion Skalierung kann von der Website \citep{google:developers}
heruntergeladen werden.

Google Cardboard bietet ein kostenloses und quelloffenes Software
Development Kit (SDK) für Entwickler, um Apps zu erstellen, die mit der
Google Cardboard VR-Plattform kompatibel sind. Das Cardboard SDK
unterstützt wichtige VR-Funktionen wie Bewegungsverfolgung,
stereoskopisches Rendering und Benutzerinteraktion über die Viewer-Taste.

Es gibt auch speziell für Google Cardboard entwickelte Apps und Spiele,
die den Nutzern interaktive Erfahrungen bieten, darunter viele
Bildungs-Apps, die sich für den Einsatz im Unterricht eignen.
\citep{google:google-cardboard}

\paragraph{Google Daydream}
Google Daydream ist ein Beispiel für ein Adapter Device. Es war
ein Virtual Reality-Headset für Android-Geräte, das von Google
entwickelt wurde. Im Gegensatz zum erschwinglichen Google
Cardboard lag hier der Fokus auf einer längeren und
professionelleren Nutzung. Neben der Software und dem
Head-Mounted Display verfügte Goo§gle Daydream über einen
Wii-ähnlichen Motion-Controller, mit dem Benutzer Objekte und
Menüs in der virtuellen Realität steuern konnten. Es war
kompatibel mit ausgewählten Smartphones, die das mobile
Betriebssystem Android (Version „Nougat“ 7.1 und höher)
verwendeten. Die Smartphones konnten einfach in das Headset
eingesetzt werden, wodurch die Inhalte des Telefons mithilfe der
Linsen in der VR betrachtet werden konnten. Je nach Auflösung des
Telefons war es möglich, eine solide und beeindruckende
Virtual-Reality-Erfahrung zu erleben. Das Headset bestand aus
weichem Stoff, der jedes kompatible Handy in ein VR-Headset
verwandelte, und war mit einem eigenen Wii-ähnlichen
Motion-Controller ausgestattet, um die Steuerung von Objekten und
Menüs in der VR zu ermöglichen.
\citep{wikipedia:google-daydream}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.5\linewidth]{google-daydream.jpg}
    \captionof{figure}{Google Daydream mit eingefügtem Smartphone und einem Wii-ähnlichen Controller. Roberts. (2017). Google Daydream}
    \label{fig:google-daydream}
\end{figure}

\paragraph{Meta Quest 3}
Die Meta Quest 3 baut auf dem Erfolg des Quest 2 auf und
kombiniert die Standalone-VR-Leistung mit der Mixed-Reality -
die sowohl für eine kabelgebundene als auch drahtlose (\glqq{}Air Link\grqq{})
PC-Verbindung möglich ist. Dank des verbesserten Displays und der
höheren Auflösung bietet das Meta Quest 3 ein noch intensiveres
visuelles Erlebnis, das die Sinne fesselt und den Benutzer noch
tiefer in die virtuelle Welt eintauchen lässt. Die eingebauten
Sensoren und Kameras ermöglichen eine präzise Bewegungsverfolgung,
wodurch ihre Bewegungen in der virtuellen Realität nahtlos und
natürlich wirken.

Details:
\begin{itemize}
\item\textbf{LCD-Displays.} Zwei separate LCD-Displays mit einer Auflösung
    von jeweils 2.064 x 2.208 Pixeln, bringen die Erfahrung auf den nächsten
    Level.
\item\textbf{Sichtfeld.} Ein Sichtfeld von 110° horizontal und 96° vertikal,
    lässt vergessen, dass man in einer virtuellen Realität ist.
\item\textbf{Pancake-Linsen.} Die Verwendung von Pancake-Linsen für die
    Optik geben dem Nutzer eine bessere Erfahrung.
\item\textbf{Touch Plus Controller.} Zwei Touch Plus Controller, welche die
    Sinne stimulieren, sind im Set des VR-Headsets enthalten.
\item\textbf{6 Kameras und 1 Sensor.} Vier Tracking-Kameras (schwarz-weiss),
    zwei 4-Megapixel-Farbkameras und ein Tiefensensor, bringen die Technologie
    zum Aufleuchten.
\item\textbf{Benutzeroberfläche.} Die intuitive Benutzeroberfläche macht
    das Einrichten einfach und schnell.
\item\textbf{Design.} Ein bequemes und elegantes Design macht die Nutzung
    umso begehrter.
\item\textbf{Akkulaufzeit.} Eine bessere Akkulaufzeit macht das Erlebnis
    immersiver.
\end{itemize}

\citep{gamesradar:best-vr-headset}

\section{Plattformen}

Plattformen sind Software-Ökosysteme und Betriebssysteme, die
XR-Erfahrungen unterstützen. Sie bieten die notwendigen
Schnittstellen und Tools, mit denen Entwickler Inhalte erstellen
und Nutzer diese Inhalte erleben können.

Plattformen kann man unterscheiden in spezifische Plattformen, zu
denen man sich nur mit spezifischen Geräten verbinden kann, wie
Oculus Home, Microsofts Windows und Apple ARKit und
systemübergreifende Plattformen, wie SteamVR 2.0 Mixed Reality
und WebXR.

\paragraph{SteamVR 2.0}
SteamVR 2.0 ist das VR-Plugin für die beliebte Steam-Plattform.
Die Steam-Plattform kann sowohl für den Erwerb von Spielen als auch
von Anwendungen genutzt werden.
Ein weiteres Highlight ist die nahtlose Integration von
SteamVR 2.0 mit verschiedenen VR-Headsets, was den Nutzern
Flexibilität und Auswahl bietet. Mit der wachsenden Bibliothek an
Inhalten, die alles von actiongeladenen Spielen bis hin zu
entspannenden VR-Erfahrungen abdeckt, bleibt SteamVR 2.0 eine der
führenden Plattformen in der Welt der virtuellen Realität.
Dank der kontinuierlichen Updates und Verbesserungen, von der
Video Game Entwicklungsfirma Valve können, Benutzer auf eine
breite Palette von VR-Titeln zugreifen.
\citep{corsera:platforms}

\paragraph{WebXR}
WebXR ist eine aufstrebende Web-Spezifikation, die die Entwicklung
von Webanwendungen ermöglicht, die über verschiedene Geräte
zugänglich sind, was einen bedeutenden Schritt für
plattformübergreifende Arbeit darstellt. Im Bereich der
erweiterten Realität (AR) sind auf der Smartphone-Seite ARKit und
ARCore relevant. Diese Technologien erlauben den Zugriff auf
Geräte wie iPhone, iPad und potenziell zukünftig auch Apple Vision Pro.
Auf der Android-Seite findet man ARCore für Android-Geräte, das
mit einer Vielzahl von Android-Smartphones kompatibel ist, und
die entsprechenden Anwendungen werden über den Play Store
verbreitet. Microsoft bietet wiederum die HoloLens an, wobei die
zugehörigen Anwendungen im Windows Store erhältlich sind.
\citep{mozilla:fundamentals-webxr}

\subsection{Anwendungen}

Diese Klasse bezieht sich auf die eigentlichen XR-Erfahrungen, die
Benutzer durch die Geräte und Plattformen erleben können.
Anwendungen sind Softwareprogramme und können vielfältig sein,
von Spielen (wie Beat Saber) und Unterhaltung (Snapchat) über
Bildungsangebote bis hin zu industriellen und medizinischen
Anwendungen. Zum Beispiel ermöglichen XR-Anwendungen Chirurgen,
komplexe Operationen zu üben, oder Ingenieuren, Prototypen in
einer virtuellen Umgebung zu testen.

Weitere Beispiele bieten IKEA und Amazon Shopping integrierte
AR-Ansichten an. Mit einem einfachen Klick können Benutzer ihre
Umgebung erkunden und Objekte in AR betrachten. So kann man testen,
ob Möbel in die eigene Wohnung passen oder eine Vorschau auf ein
Produkt sehen, das man bei Amazon kaufen möchte.

\subsubsection{Apps Beispiele die man in der Schule einsetzen kann}

\paragraph{SketchAR}
SketchAR ist eine innovative Anwendung und Plattform, die speziell
für kreative Köpfe konzipiert wurde. Diese einzigartige Verbindung
von Augmented Reality und künstlicher Intelligenz beschleunigt den
Lernprozess und macht ihn zugleich unterhaltsam. Die App
ermöglicht das Zeichnen in Augmented Reality, die Umwandlung von
Fotos in Illustrationen, interaktive Minispiele zur Förderung der
Kreativität, Kurse mit Zeichenlektionen, ein Zeichentool mit
schrittweiser Anleitung sowie eine Funktion zum Teilen auf
verschiedenen Plattformen und \acrshort{NFT}s für beeindruckende
Kunstwerke. Für den Zugriff auf exklusive Tools und uneingeschränkten
Funktionsumfang stehen In-App-Käufe zur Verfügung.

\subsection{Werkzeuge (Tools)}

Hierbei handelt es sich um die Entwicklungswerkzeuge und Software,
die benötigt werden, um XR-Inhalte zu erstellen. Diese Werkzeuge
erleichtern Entwicklern das Design, die Programmierung und das
Testen von XR-Anwendungen. Beliebte Werkzeuge in diesem Bereich
sind Unity, Unreal Engine, Blender und Vuforia.
\citep{michael-nebeling:augmented}

\section{Schlüsselkonzepte der VR}

Diese Schlüsselkonzepte sind grundlegend für das Verständnis und
die Gestaltung von Virtual Reality (VR)-Erlebnissen.

\subsection{Präsenz \& Immersion}

Präsenz und Immersion in der VR-Technologie verändern grundlegend,
wie wir digitale Umgebungen erleben. VR schafft durch die Simulation
realer Umgebungen und Interaktionsmöglichkeiten ein intensives
Präsenzgefühl. Dieses starke Gefühl des \glqq{}Dabeiseins\grqq{} wird
durch hochauflösende Bilder, räumliches Audio und intuitive Steuerungen,
die natürliche Bewegungen nachahmen, erzeugt.

In der Bildung ermöglicht VR den Schülern, historische Stätten zu
erkunden, komplexe wissenschaftliche Konzepte zu erforschen und neue
Fähigkeiten in einer sicheren Umgebung zu erproben.
\citep{michael-nebeling:augmented}

\subsection{Embodiment und Avatare}

In der virtuellen Realität bieten Embodiment und Avatare den Nutzern
eine einzigartige und transformative Erfahrung, um in verschiedene
Identitäten und Perspektiven einzutauchen. Avatare fungieren als digitale
Darstellungen von uns selbst, die es erlauben, in immersiven Umgebungen zu
interagieren und zu navigieren. Durch den technologischen Fortschritt werden
die Realitätsnähe und Reaktionsfähigkeit der Avatare kontinuierlich verbessert,
was diese Erlebnisse noch faszinierender macht. Entwickler experimentieren mit
haptischem Feedback, Eye-Tracking und fortschrittlicher Bewegungserfassung,
um realistischere und reaktionsschnellere Avatare zu gestalten. Dies trägt nicht
nur zu einem tieferen Eintauchen in die virtuelle Welt bei, sondern ermöglicht
auch feinere Ausdrücke und Interaktionen. \citep{michael-nebeling:augmented}

\subsection{Kognition \& Empathie}

Durch VR können komplexe Themen besser verstanden, emotionale Bindungen gefördert
und sichere Räume für therapeutische Anwendungen geschaffen werden. Schüler können
historische Ereignisse erleben, während Therapiepatienten Ängste bewältigen und
Empathie trainieren können. \citep{michael-nebeling:augmented}

\subsection{Proprioception \& Motion Sickness}

Die Propriozeption wird oft als „sechster Sinn“ bezeichnet und ermöglicht uns,
alltägliche Aktivitäten auszuführen, ohne ständig auf unsere Gliedmassen zu achten.
Sie stützt sich auf Rezeptoren in unseren Muskeln, Sehnen und Gelenken, die Signale
über Körperposition und Bewegung an das Gehirn senden.

In der virtuellen Realität (VR) kann dieses fein abgestimmte System jedoch aus dem
Gleichgewicht gebracht werden. VR schafft immersive Erfahrungen, die das Gehirn
visuell davon überzeugen können, dass wir uns bewegen, auch wenn wir stillstehen.
Diese Diskrepanz zwischen visuellem Input und körperlichen Empfindungen kann zur
Reisekrankheit führen, die sich durch Symptome wie Schwindel, Übelkeit und
Desorientierung äussert.

Forscher suchen ständig nach Möglichkeiten, die \glqq{}Seekrankheit\grqq{} in
der VR zu minimieren.
Zu den Strategien gehören die Verbesserung der Bildwiederholrate und der Auflösung von
VR-Displays usw. \citep{michael-nebeling:augmented}

\subsection{Haptik \& Pseudo-Haptik}

Haptische Technologie simuliert den Tastsinn in der virtuellen Realität durch taktiles
Feedback mittels Handschuhen, Anzügen oder Controllern mit Aktoren. Dies ermöglicht es
Benutzern, virtuelle Objekte realistisch zu \glqq{}fühlen\grqq{} und ihre Interaktionen
zu verbessern.
Pseudohaptik erzeugt die Illusion von Berührungen durch visuelle und auditive Hinweise,
die taktile Empfindungen im Gehirn auslösen. Änderungen in der visuellen Darstellung
eines Objekts und synchronisierte Soundeffekte verstärken diese Wahrnehmung.
\citep{michael-nebeling:augmented}

\subsection{Boundary \& Redirected Walking}

Bei Begrenzungstechniken geht es darum, den Spielbereich zu definieren und mit virtuellen
Barrieren zu markieren, um sicherzustellen, dass Benutzer nicht mit realen Hindernissen
kollidieren.

Im Gegensatz dazu wird beim umgeleiteten Gehen subtile Manipulationen der virtuellen
Umgebung genutzt, um Benutzer auf bestimmte Pfade zu lenken und ihnen das Erkunden eines
grösseren Gebiets vorzutäuschen. \citep{michael-nebeling:augmented}
