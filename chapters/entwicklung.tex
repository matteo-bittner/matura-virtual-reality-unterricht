\selectlanguage{ngerman}%
\chapter{Entwicklung in einem VR-Lab}%
\label{chap:entwicklung}

Dieser Abschnitt befasst sich mit den Möglichkeiten, VR-Lerninhalte im
Schulbetrieb selbst zu kreieren für solchen Schulstoff, für den diese
noch nicht vorhanden oder zu beschaffen sind. Insbesondere interessiert
uns die konkrete Umsetzung der Erstellung und der Aufwand für solche
Aktivitäten, um beurteilen zu können, ob dies im Schulalltag im Rahmen
von regulären Fächern oder eines Freifachs realistisch umsetzbar ist.

\section{VR-Lab}

Ein VR-Lab wäre ein Freifach, in dem sich Schülerinnen und Schüler mit dem Thema
\acrshort{VR}, und zwar konkret mit den Lerninhalten, Lernprozessen und
den damit verbundenen Technologien beschäftigen. Ziele des Freifachs können
sein:

\begin{itemize}
\item\textbf{Wartung der Hardware und Software} (Aktualisierungen, Kauf, Miete,
    Leasing, Aushandeln und Betreuen von Wartungsverträgen)
\item\textbf{Wartung der Bibliothek von Lerninhalten} (Überprüfung, Aktualisierung,
    Weiterentwicklung)
\item\textbf{Testen und Auswahl von Lerninhalten} (bei Ankauf und bei Recherche von
    freien Inhalten)
\item\textbf{Entwicklung von Lerninhalten} (Eigenentwicklung, Betreuung von Agenturen
    bei externer Beauftragung)
\item\textbf{Koordination mit anderen Schulen} (Ideen holen, entwickelte Inhalte
    teilen, Entwicklungs- oder Anschaffungskosten teilen, Crowdfunding)
\end{itemize}

Wenn diese Aktivitäten in Zusammenarbeit mit den betroffenen Lehrpersonen
und anderen Fachkräften (z.B. IT-Support, Administration) durchführt
werden, lernen die Jugendlichen Verantwortung zu tragen und kompromissfähig
belastbare Entscheidungen zu treffen.

Wird das Lehrmaterial selbst erstellt, können die Jugendlichen ihre
Interessen vertiefen und eigene Stärken identifizieren (z.B. grafische
Gestaltung, Programmierung, Qualitätskontrolle, Koordination von Aufträgen
und externen Lieferanten). Ausserdem können selbstproduzierte Inhalte
helfen, als Schule in Bezug auf Lehrmittel autark oder zumindest
unabhängiger zu werden, d.h. konkret die heute weit verbreitete Abhängigkeit
von Big Tech zu reduzieren.

\section{Gründe für VR-Labs}

VR-Labs sind Orte des kreativen oder strukturierten Erschaffens. Natürlich
kann man ausreichend Argumente dafür finden, dass es -- ökonomisch und
pädagogisch -- klüger ist, sich rein auf die Nutzung von bestehenden
Inhalten im Unterricht zu konzentrieren, also den eigentlichen Zweck der
Anschaffung der Technologie, und das Erstellen derartiger Inhalte
professionellen Anbietern zu überlassen.

Wird die Entwicklungsarbeit allerdings Schülerinnen und Schüler überlassen, ergeben
sich zahlreiche positive Effekte, die über das reine Lernen von Schulstoff
hinausgehen.

\begin{itemize}
\item\textbf{Praktisches Lernen.} Die Möglichkeit, praktische Erfahrungen
    in der Projektabwicklung, Softwareentwicklung, im Design und in der
    Programmierung zu sammeln. Dies fördert wichtige Fähigkeiten, die in
    der heutigen digitalen Welt von Bedeutung sind.
\item\textbf{Teamarbeit.} Die Entwicklung von VR-Inhalten kann Teamarbeit
    entstehen lassen. Jugendliche lernen dann, effektiv zusammenzuarbeiten,
    ihre Ideen zu kommunizieren und Probleme gemeinsam zu lösen.
\item\textbf{Motivation.} Gesteigertes Interesse an den Unterrichtsthemen,
    da die Jugendlichen sich mit den selbst verwalteten oder erstellten
    Inhalten identifizieren. Das Lernen wird intrinsischer.
\item\textbf{Einzigartige Inhalte.} Durch kreative, innovative Ideen können
    einzigartige Inhalte entstehen, die in hohem Mass Interesse im
    Schulbetrieb wecken können und auf diese Art von grossen, anonymen
    Anbietern vermutlich nicht angeboten werden.
\item\textbf{Feedback und Reflexion.} Jugendliche können durch den
    Entwicklungsprozess wachsen, indem sie mit Feedback zu Inhalten, die
    nicht so gut ankommen, umgehen lernen und ihre Arbeiten kritisch
    reflektieren, um Verbesserungen vorzunehmen.
\item\textbf{Anpassungen.} Inhalte können so entwickelt oder angepasst
    werden, dass sie auf eigene Interessen und Bedürfnisse Rücksicht nehmen.
    Auch die Aktualisierung veralteter Inhalte ist möglich und verlängert
    den Nutzen derselben.
\item\textbf{Gesellschaftswandel.} Jugendliche erleben, wie Technologie
    genutzt werden kann, um selbständig zu gestalten und dadurch unabhängig
    zu werden oder zu bleiben.
\item\textbf{Knowledge Management.} Jugendliche erleben selbst, wie
    Wissenserwerb in einem Unternehmen organisiert und gestaltet werden kann.
\end{itemize}

Insgesamt kann die Überlassung der Entwicklungsarbeit im Rahmen eines
Freifachs also eine wertvolle Bildungsressource sein, die nicht nur
technische Fähigkeiten fördert, sondern auch Kreativität und Engagement im
Lernprozess stärkt.

Es ist einleuchtend, dass ein erfolgreicher Betrieb eines VR-Labs viel
Arbeit für Schülerinnen und Schüler und Lehrpersonal bedeutet. Der Nutzen ist aber
derart mannigfaltig, dass sich die Investition zweifellos lohnt.

\section{Die Arbeit im Lab}

Zum Erstellen von Inhalten in VR-Labs gibt es grundsätzlich drei Alternativen
mit unterschiedlichem Einarbeitungsaufwand und Schwierigkeitsgrad: Entwickeln
für den Webbrowser auf Basis von Web Standards, Entwickeln von
plattformübergreifenden Lösungen und Entwickeln von nativen, meist mobilen
Inhalten speziell für die XR-Endgeräte.

\paragraph{Web Standards}
\gls{webxr} ist ein Standard für eine Programmierschnittstelle (\acrshort{API})
von XR-Geräten, der in modernen Webbrowsern zur Verfügung steht. Zur
Programmierung verwendet man in der Regel freie JavaScript-Frameworks (z.B.
A-Frame, Babylon.js, Three.js).

Die Hauptvorteile dieses Ansatzes sind, dass man direkt in der Zielplattform,
nämlich im Webbrowser, entwickeln kann und dass man zum Bereitstellen der
Applikation an keine App Stores gebunden ist. Man benötigt aber einen
Webspace, wo die Applikation wie eine Website online gestellt wird.
Wer hingegen gewohnt ist, zur Erstellung von Inhalten mit einem Grafikprogramm
zu arbeiten, muss an die Arbeitsweise mit Quellcode und Webservern erst
gewöhnen. \citep{immersiveweb:dev}

\paragraph{Plattformübergreifend}
Für das Erstellen von Inhalten, die optimiert mit nativer Performance auf
unterschiedlichen Betriebssystemen und Geräten laufen, gibt es eine Reihe
von Entwicklungs- und Laufzeitplattformen und ihre Entwicklungsumgebungen.
Die bekanntesten davon sind \product{Unity} und die \product{Unreal Engine}.

Das Versprechen der plattformübergreifenden Entwicklung ist, dass man native
Features für viele Endgeräte frei Haus bekommt. Man entwickelt also einen
Inhalt und kann diesen dann auf allen verbreiteten XR-Geräte zur Verfügung
stellen. Der Nachteil ist, dass man in der Regel an App Stores gebunden ist
und die Qualitätsrichtlinien dieser Stores einhalten muss, um eine
entwickelte App zu veröffentlichen.

Wir betrachten im Anschluss in \autoref{subsec:unity} und \autoref{subsec:unreal}
diese beiden Produkte etwas genauer.

\paragraph{Nativ}
Für die direkte Entwicklung nativer Applikation, die auf XR-Geräten wie der
\product{Apple Vision Pro} oder der \product{Meta Quest 3} laufen sollen,
gibt es von den Herstellern speziell entwickelte Software Development Kits
(\acrshort{SDK}) und Entwicklungsumgebungen.
\citep{apple:vision-os,oculus:docs-native}

Unter macOS steht zur Entwicklung z.B. die \acrshort{IDE} \product{Xcode}
zur Verfügung. Zur Entwicklung für Android-basierte Endgeräte wird für
Linux, macOS und Windows die \product{Android Studio IDE} empfohlen.
\citep{oculus:docs-android}

Diese Art der Entwicklung ähnelt dem Entwickeln von mobilen Applikationen
und ist nötig, um alle Fähigkeiten und Feinheiten des Endgerätes zu nutzen,
die z.B. mit plattformübergreifend arbeitenden Entwicklungstools nicht
ausgereizt werden können.

\subsection{Unity}%
\label{subsec:unity}

\product{Unity} ist eine plattformübergreifende Laufzeit- und
Entwicklungsumgebung für Spiele und ähnliche 3D-Inhalte, d.h. eine Software,
die es ermöglicht, auf unterschiedlichen Betriebssystemen interaktive
Inhalte zu entwickeln und zu konsumieren. Die Plattform wurde 10 Jahre nach
dem Start der \product{Unreal Engine} auf den Markt gebracht.%
\citep{wikipedia:unity}

Der Unity Editor ist eine integrierte Entwicklungsumgebung (\acrshort{IDE}),
mit der man 2D- und 3D-Spiele und Welten erschaffen kann. Man kann das
Programm kostenlos von der Website des Herstellers herunterladen. Man muss
dazu zuerst den \product{Unit Hub} installieren.%
\citep{unity:store-download}

\begin{figure}[ht]
    \centering
   \begin{minipage}{.4\linewidth}
      \includegraphics[width=\linewidth]{how-install-unity.png}
      \captionof{figure}{Anleitung zur Installation von Unity. Eigenes Werk. (2024)}
    \label{fig:install-unity}
   \end{minipage}
   \hspace{.05\linewidth}
   \begin{minipage}{.4\linewidth}
      \includegraphics[width=\linewidth]{how-install-unity-linux.png}
      \captionof{figure}{Anleitung zur Installation von Unity auf Linux. Eigenes Werk. (2024)}
    \label{fig:install-unity-linux}
   \end{minipage}
  \end{figure}

Die Benutzeroberfläche des Editors erinnert einen an ein typisches
Bildbearbeitungsprogramm, wie z.B. GIMP oder Photoshop. Zentral in der Mitte
befindet sich die Arbeitsfläche mit den erstellten Inhalten, an den Seiten
und am unteren Rand sind sogenannte Werkzeuge platziert.

Zum Erstellen von VR- und AR-Inhalten muss man ein paar Konzepte lernen.
Das lässt sich aber gut mit Online-Tutorials erledigen.

\begin{figure}[ht]
    \centering
   \begin{minipage}{.4\linewidth}
      \includegraphics[width=\linewidth]{starting-unity-project.png}
      \captionof{figure}{Anleitung zur Installation von Unity. Eigenes Werk. (2024)}
    \label{fig:opening-unity}
   \end{minipage}
   \hspace{.05\linewidth}
   \begin{minipage}{.4\linewidth}
      \includegraphics[width=\linewidth]{opened-unity-project.png}
      \captionof{figure}{Anleitung zur Installation von Unity auf Linux. Eigenes Werk. (2024)}
    \label{fig:opened-unity}
   \end{minipage}
  \end{figure}

Am Schluss muss man im Unity Editor von seinem Projekt ein installierbares
Paket bauen und dieses auf die VR-Brille hochladen. Alles in allem ist für
Einarbeitung etwas Zeit notwendig.
\citep{oculus:docs-blog-metaquest3,unity:blog-metaquest3}

\subsection{Unreal Engine}%
\label{subsec:unreal}

\product{Unreal Engine} ist der ursprüngliche Platzhirsch am Markt und wie
\product{Unity} eine plattformübergreifende Laufzeit- und Entwicklungsumgebung
für Spiele und ähnliche 3D-Inhalte. Die Plattform ist vom Konzept her sehr
ähnlich aufgebaut, man hat auch hier einen Hub, den \product{Epic Games Launcher},
aber tatsächlich kann man die Unreal Engine auch ohne diesen herunterladen
und installieren, insbesondere für Linux.

\begin{figure}[ht]
    \centering
   \begin{minipage}{.4\linewidth}
      \includegraphics[width=\linewidth]{how-install-unreal.png}
      \captionof{figure}{Anleitung zur Installation von Unreal Engine auf Linux. Eigenes Werk. (2024)}
    \label{fig:install-unreal}
   \end{minipage}
   \hspace{.05\linewidth}
   \begin{minipage}{.4\linewidth}
      \includegraphics[width=\linewidth]{opening-unreal-software.png}
      \captionof{figure}{Starten von Unreal Engine auf Linux. Eigenes Werk. (2024)}
    \label{fig:opening-unreal}
   \end{minipage}
  \end{figure}

Auch hier lehnt sich die Benutzeroberfläche stark an Bildbearbeitungsprogramme
an. Der Arbeitsbereich befindet sich zentral in der Mitte, an den Seiten
und am unteren Rand befinden sich Paneele mit Werkzeugen. Als Overlay am
oberen Rand des Arbeitsbereichs sind die zentralen Arbeitsinstrumente
integriert.

\begin{figure}[ht]
    \centering
   \begin{minipage}{.4\linewidth}
      \includegraphics[width=\linewidth]{start-project-unreal.png}
      \captionof{figure}{Kreieren eines Unreal Engine Projekts unter Linux. Eigenes Werk. (2024)}
    \label{fig:create-project-unreal}
   \end{minipage}
   \hspace{.05\linewidth}
   \begin{minipage}{.4\linewidth}
      \includegraphics[width=\linewidth]{opened-project-unreal.png}
      \captionof{figure}{Über Unreal Engine geöffnetes Projekt auf Linux. Eigenes Werk. (2024)}
    \label{fig:working-with-unreal}
   \end{minipage}
  \end{figure}

Zum schnellen Starten bietet auch Unreal ein Reihe von Projektvorlagen an,
dazu eigene für XR. Man kann ein Projekt lokal am Computer ausprobieren,
aber um es am Headset zu testen, muss man es auf das VR-Gerät hochladen.
Dazu kann man das Headset, gemäss offizieller Dokumentation, mit einem
passenden USB-Kabel verbinden und direkt von der IDE hochladen.
\citep{oculus:docs-unreal-quickstart}

Auch hier gilt, zur Einarbeitung braucht man Zeit und Geduld. Es stehen dazu
neben der offiziellen Dokumentation auch ausreichend Tutorials im Internet
zur Verfügung.
