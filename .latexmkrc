# taken from https://tex.stackexchange.com/a/44316

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
    # handle -outdir param by splitting path and file, ...
    my ($base_name, $path) = fileparse( $_[0] );
    pushd $path; # ... cd-ing into folder first, then running makeglossaries ...

    if ( $silent ) {
        system "makeglossaries -q '$base_name'";  # Unix
        # system "makeglossaries", "-q", "$base_name";  # Windows
    }
    else {
        system "makeglossaries '$base_name'";  # Unix
        # system "makeglossaries", "$base_name";  # Windows
    };

    popd; # ... and cd-ing back again
}

push @generated_exts, 'glo', 'gls', 'glg', 'glsdefs';
push @generated_exts, 'acn', 'acr', 'alg';
push @generated_exts, 'bbl', 'bcf', 'blg';
push @generated_exts, 'bbl-SAVE-ERROR';
push @generated_exts, 'run.xml';
push @generated_exts, 'synctex.gz';
$clean_ext .= ' %R.bbl %R.ist %R.xdy';
