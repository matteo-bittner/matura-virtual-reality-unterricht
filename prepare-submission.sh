#!/usr/bin/bash -e

echo '=== 1. Prepare document for plagiarism check ==='
for FILE in *.bib $(find -name '*.tex'); do
    sed -i -E 's/(Matteo Bittner|matteo-bittner|Marcello Indino|Bittner|Indino)/(redacted)/' $FILE
    sed -i -E 's/^( +|)\\includegraphics.*/(image-removed)/' $FILE
done
sed -i '/printbibliography/d' virtual-reality-unterricht.tex

if which pandoc > /dev/null
then
    echo '=== 2. Build DOCX document for plagiarism check ==='
    pandoc virtual-reality-unterricht.tex --bibliography=virtual-reality-unterricht.bib -o virtual-reality-unterricht.docx

    echo '=== 3. Rename DOCX document for plagiarism check ==='
    mv -v virtual-reality-unterricht.docx 'MaPl202425McVirtualRealityUnterricht.docx'

    echo '=== 4. Restore checked-in state of .tex files ==='
    if which git > /dev/null
    then
        git checkout appendix/*.tex chapters/*.tex *.tex *.bib
        git status
    else
        echo 'WARNING: Git not found, must leave modified files in the source tree.'
        echo 'HINT: Run `git checkout <filename>` to restore the original documents.'
    fi

    echo '=== 5. DONE ==='
    ls -1F --color *.docx
else
    echo '========================================================================='
    echo 'WARNING: Pandoc not found. Skipping DOCX creation. ======================'
    echo '========================================================================='
fi

if which latexmk > /dev/null
then
    echo '=== 2. Build PDF document for plagiarism check ==='
    latexmk -c
    latexmk -pdf

    echo '=== 3. Rename PDF document for plagiarism check ==='
    mv -v virtual-reality-unterricht.pdf 'MaPl202425McVirtualRealityUnterricht.pdf'

    echo '=== 4. Restore checked-in state of .tex files ==='
    git checkout appendix/*.tex chapters/*.tex *.tex *.bib

    echo '=== 5. Build regular document ==='
    latexmk -pdf

    echo '=== 6. Rename regular document ==='
    mv -v virtual-reality-unterricht.pdf 'MaOr 2024 25Mc Bittner Matteo.pdf'

    echo '=== 7. Clean up debris from LaTeX build ==='
    latexmk -c

    echo '=== 8. DONE ==='
    git status
    ls -1F --color *.pdf
else
    echo '========================================================================='
    echo 'WARNING: LaTeX build command not found. Skipping PDF creation. =========='
    echo '========================================================================='
fi
